<?php get_header(); ?>
	

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>				

		<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_photo'); echo $image['url']; ?>);">
			<div class="content">
				<div class="wrapper">

					<div class="info">
						<h1><?php the_title(); ?></h1>
					</div>

				</div>
			</div>
		</section>

		<section id="profile">
			<div class="wrapper">


				<article>
					<?php the_content(); ?>
				</article>

				<aside id="vitals">

					<?php if(get_field('twitter') || get_field('instagram')): ?>
						<div class="social">
							<?php if(get_field('twitter')): ?>
								<a href="<?php the_field('twitter'); ?>" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/twitter-profile.svg" alt="Twitter" /></a>
							<?php endif; ?>

							<?php if(get_field('instagram')): ?>
								<a href="<?php the_field('instagram'); ?>" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/instagram-profile.svg" alt="Instagram" /></a>
							<?php endif; ?>
						</div>
					<?php endif; ?>

					<?php if(get_field('place_of_birth')): ?>
						<h3>Place of Birth</h3>
						<p><?php the_field('place_of_birth'); ?></p>
					<?php endif; ?>

					<?php if(get_field('hometown')): ?>
						<h3>Hometown</h3>
						<p><?php the_field('hometown'); ?></p>
					<?php endif; ?>

					<?php if(get_field('current_residence')): ?>
						<h3>Current Residence</h3>
						<p><?php the_field('current_residence'); ?></p>
					<?php endif; ?>

					<?php if(get_field('birthdate')): ?>
						<h3>Birthdate</h3>
						<p><?php the_field('birthdate'); ?></p>
					<?php endif; ?>

					<?php if(get_field('club')): ?>
						<h3>Club</h3>
						<p><?php the_field('club'); ?></p>
					<?php endif; ?>

					<?php if(get_field('college')): ?>
						<h3>College</h3>
						<p><?php the_field('college'); ?></p>
					<?php endif; ?>

				</aside>


			</div>
		</section>

	<?php endwhile; endif; ?>


<?php get_footer(); ?>