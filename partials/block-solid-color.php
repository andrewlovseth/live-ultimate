<div class="feature solid-color <?php the_sub_field('color_theme'); ?>">
	<div class="content">

		<div class="info">
			<?php get_template_part('partials/block-headline'); ?>
			
			<?php the_sub_field('deck'); ?>

			<?php get_template_part('partials/block-cta'); ?>
		</div>
	</div>
</div>