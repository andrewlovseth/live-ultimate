<div class="feature cover background-image <?php the_sub_field('color_theme'); ?>" style="background-image: url(<?php $bgImage = get_sub_field('background_image'); echo $bgImage['url']; ?>);">
	<div class="content">

		<div class="info">
			<?php get_template_part('partials/block-headline'); ?>
			
			<?php the_sub_field('deck'); ?>

			<?php get_template_part('partials/block-cta'); ?>
		</div>
	</div>
</div>