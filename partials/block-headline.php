<?php if(get_sub_field('headline_type') == 'text'): ?>
	<h3 class="text-headline"><?php the_sub_field('text_headline'); ?></h3>
<?php endif; ?>

<?php if(get_sub_field('headline_type') == 'graphic'): ?>
	<img class="graphic-headline" src="<?php $image = get_sub_field('graphic_headline'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
<?php endif; ?>