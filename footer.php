	<footer>
		<div class="wrapper">

			<a href="http://usaultimate.org/" rel="external" class="usau-logo">
				<img src="<?php bloginfo('template_directory') ?>/images/footer-logo.png" alt="USA Ultimate" />
			</a>

			<div class="social">
				<a href="<?php the_field('facebook', 'options'); ?>" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/facebook.svg" alt="Facebook" /></a>
				<a href="<?php the_field('twitter', 'options'); ?>" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/twitter.svg" alt="Twitter" /></a>
				<a href="<?php the_field('instagram', 'options'); ?>" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/instagram.svg" alt="Instagram" /></a>
				<a href="<?php the_field('youtube', 'options'); ?>" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/youtube.svg" alt="YouTube" /></a>
				<a href="<?php the_field('snapchat', 'options'); ?>" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/snapchat.svg" alt="Snapchat" /></a>
			</div>

			<p><?php the_field('copyright', 'options'); ?></p>

		</div>
	</footer>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109661344-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	 
	  gtag('config', 'UA-109661344-1');
	</script>

</body>
</html>