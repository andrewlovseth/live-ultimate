<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="hero">

		<?php if(have_rows('hero_slides')): while(have_rows('hero_slides')): the_row(); ?>

			<?php if(get_sub_field('link')): ?>
				<a href="<?php the_sub_field('link'); ?>">
			<?php endif; ?>

				<article class="cover" style="background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>);">
					<div class="content">

						<div class="wrapper">
							<div class="info">
								<h2><?php the_sub_field('headline'); ?></h2>
							</div>
						</div>

					</div>
				</article>

			<?php if(get_sub_field('link')): ?>
				</a>
			<?php endif; ?>

		<?php endwhile; endif; ?>
			
	</section>

	<section id="features">

		<?php if(have_rows('blocks')): while(have_rows('blocks')) : the_row(); ?>
		 
		    <?php if( get_row_layout() == 'background_image' ): ?>

		    	<?php get_template_part('partials/block-background-image'); ?>
				
		    <?php endif; ?>

		    <?php if( get_row_layout() == 'solid_color' ): ?>

		    	<?php get_template_part('partials/block-solid-color'); ?>

		    <?php endif; ?>
		 
		<?php endwhile; endif; ?>	

	</section>

<?php get_footer(); ?>