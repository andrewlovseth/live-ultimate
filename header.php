<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet">
	<link rel="stylesheet" href="https://use.typekit.net/oga8kmm.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="<?php echo $post->post_name; ?>">

	<?php get_template_part('partials/survey'); ?>

	<header>
		<div class="wrapper">

			<div id="header-logo">
				<a href="http://usaultimate.org/" rel="external" class="usau-logo">
					<img src="<?php bloginfo('template_directory') ?>/images/footer-logo.png" alt="USA Ultimate" />
				</a>

				<h1>
					<a href="<?php echo site_url('/'); ?>">
						<span class="live">Live</span>
						<span class="ultimate">Ultimate</span>
					</a>
				</h1>
			</div>

			<a href="#" id="toggle">
				<div class="patty"></div>
			</a>

			<nav>
				
				<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
				 
				    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>

				<?php endwhile; endif; ?>

			</nav>

		</div>
	</header>