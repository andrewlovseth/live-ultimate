<?php get_header(); ?>

	<section id="header">
		<div class="wrapper">

			<h1>Ambassadors</h1>

		</div>
	</section>

	<section id="list">
		<div class="wrapper">

			<?php $posts = get_field('ambassadors', 'options'); if( $posts ): ?>

			    <?php foreach( $posts as $post): setup_postdata($post); ?>

					<article>
						<div class="image">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php $image = get_field('profile_photo'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>

						<div class="info">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						</div>
					</article>

			    <?php endforeach; ?>

			<?php wp_reset_postdata(); endif; ?>
			
		</div>
	</section>

<?php get_footer(); ?>