$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){
		$('header').toggleClass('open');
		$('nav').slideToggle(300);
		return false;
	});

	// Home Hero Carousel
	$('body.home #hero').slick({
		dots: true,
		arrows: false,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
	    fade: true,
	    cssEase: 'linear',
	    autoplay: true,
	    autoplaySpeed: 4000

	});



	// FitVids
	$('.embed').fitVids();

});