<?php

/*

	Template Name: About

*/

get_header(); ?>

	<section id="header">
		<div class="wrapper">

			<h1>About #LiveUltimate</h1>

		</div>
	</section>

	<section id="video">
		<div class="wrapper">

			<div class="embed">
				<iframe src="//www.youtube-nocookie.com/embed/<?php the_field('youtube_id'); ?>?rel=0&showinfo=0&modestbranding=1&vq=hd1080&autoplay=0" frameborder="0" width="1920" height="1080" allowfullscreen></iframe>
			</div>

		</div>
	</section>

	<section id="story">
		<div class="wrapper">
			
			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; endif; ?>

		</div>
	</section>

<?php get_footer(); ?>